<!-- TOC -->

- [1. 面向对象](#1-面向对象)
    - [1.1. 封装](#11-封装)
    - [1.2. 继承](#12-继承)
    - [1.3. 多态](#13-多态)
    - [1.4. 基本设计原则SOLID](#14-基本设计原则solid)
- [2. 设计模式](#2-设计模式)
    - [2.1. 创建型](#21-创建型)
    - [2.2. 结构型](#22-结构型)
    - [2.3. 行为型](#23-行为型)
- [3. CAP理论](#3-cap理论)
    - [3.1. 基本概念](#31-基本概念)
    - [3.2. ACID](#32-acid)
    - [3.3. BASE](#33-base)
- [4. 服务化](#4-服务化)
    - [4.1. 服务拆分](#41-服务拆分)
    - [4.2. 服务治理](#42-服务治理)
    - [4.3. 分布式事务](#43-分布式事务)
    - [4.4. 两阶段提交](#44-两阶段提交)
    - [4.5. 三阶段提交](#45-三阶段提交)
    - [4.6. tcc](#46-tcc)
    - [4.7. xa](#47-xa)
    - [4.8. mq最终一致性](#48-mq最终一致性)

<!-- /TOC -->


# 1. 面向对象
## 1.1. 封装
## 1.2. 继承
## 1.3. 多态
## 1.4. 基本设计原则SOLID
- S－单一职责原则
```
    单一职责原则（SRP）表明一个类有且只有一个职责。
```
- O－开放关闭原则
```
    一个类应该对扩展开放，对修改关闭。这意味一旦你创建了一个类并且应用程序的其他部分开始使用它，你不应该修改它。
```
- L－里氏替换原则
```
    派生的子类应该是可替换基类的，也就是说任何基类可以出现的地方，子类一定可以出现。
```
- I－接口隔离原则
```
    类不应该被迫依赖他们不使用的方法，也就是说一个接口应该拥有尽可能少的行为，它是精简的，也是单一的。
```
- D－依赖倒置原则
```
    高层模块不应该依赖低层模块，相反，他们应该依赖抽象类或者接口。这意味着你不应该在高层模块中使用具体的低层模块。
```

# 2. 设计模式
## 2.1. 创建型
## 2.2. 结构型
## 2.3. 行为型

# 3. CAP理论
## 3.1. 基本概念
- 定理：任何分布式系统只可同时满足二点，没法三者兼顾。
    - Consistency(一致性)
    - Availability(可用性)
    - Partition tolerance(分区容忍性)
## 3.2. ACID
- ACID模型拥有 高一致性 + 可用性 很难进行分区
    - Atomicity原子性：一个事务中所有操作都必须全部完成，要么全部不完成。
    - Consistency一致性. 在事务开始或结束时，数据库应该在一致状态。
    - Isolation隔离层. 事务将假定只有它自己在操作数据库，彼此不知晓。
    - Durability. 一旦事务完成，就不能返回。


## 3.3. BASE
- BASE模型，牺牲高一致性，获得可用性或可靠性
    - Basically Available基本可用。支持分区失败(e.g. sharding碎片划分数据库)
        - 失败重试
        - 服务降级
    - Soft state软状态 状态可以有一段时间不同步，异步。
    - Eventually consistent最终一致，最终数据是一致的就可以了，而不是时时高一致。
        - MQ
    
# 4. 服务化
## 4.1. 服务拆分
## 4.2. 服务治理
## 4.3. 分布式事务
## 4.4. 两阶段提交
## 4.5. 三阶段提交
## 4.6. tcc
- TRYING 阶段主要是对业务系统做检测及资源预留
- CONFIRMING 阶段主要是对业务系统做确认提交，TRYING阶段执行成功并开始执行CONFIRMING阶段时，默认CONFIRMING阶段是不会出错的。即：只要TRYING成功，CONFIRMING一定成功。
- CANCELING 阶段主要是在业务执行错误，需要回滚的状态下执行的业务取消，预留资源释放。
- 例子
```
    支付系统接收到会员的支付请求后，需要扣减会员账户余额、增加会员积分（暂时假设需要同步实现）增加商户账户余额
    再假设：会员系统、商户系统、积分系统是独立的三个子系统，无法通过传统的事务方式进行处理。
    TRYING阶段：我们需要做的就是会员资金账户的资金预留，即：冻结会员账户的金额（订单金额）
    CONFIRMING阶段：我们需要做的就是会员积分账户增加积分余额，商户账户增加账户余额
    CANCELING阶段：该阶段需要执行的就是解冻释放我们扣减的会员余额
```

## 4.7. xa
## 4.8. mq最终一致性
- 方案一：事务mq
    ![](_paste_img/README/2018-07-05-13-46-32.png)
    - 需要业务方提供消息check服务，MQServer定时轮询消息状态。
- 方案二：非事务mq
    ![](_paste_img/README/2018-07-05-21-18-36.png)
    - 生产者
        - 操作数据库成功，向MQ中投递消息也成功，皆大欢喜 
        - 操作数据库失败，不会向MQ中投递消息了 
        - 操作数据库成功，但是向MQ中投递消息时失败，向外抛出了异常，刚刚执行的更新数据库的操作将被回滚
    - 消费者
        - 消息出列后，消费者对应的业务操作要执行成功。如果业务执行失败，消息不能失效或者丢失。需要保证消息与业务操作一致 
        - 尽量避免消息重复消费。如果重复消费，也不能因此影响业务结果
- 方案三
    - 将业务数据和消息数据先都存在业务数据库里面，通过数据库的事务保证一致性，随后将消息转发给MQ。
- mq消息去重思路
    - 通过唯一键值做处理，即每次调用的时候传入唯一键值，通过唯一键值判断业务是否被操作，如果已被操作，则不再重复操作
    - 通过状态机处理，给业务数据设置状态，通过业务状态判断是否需要重复执行