<!-- TOC depthFrom:1 depthTo:6 orderedList:false updateOnSave:true withLinks:true -->

- [1. 理论基础](#1-理论基础)
- [2. 协议规范](#2-协议规范)
    - [2.1. http协议](#21-http协议)
        - [2.1.1. cookie](#211-cookie)
        - [2.1.2. session](#212-session)
        - [2.1.3. status code](#213-status-code)
    - [2.2. TCP](#22-tcp)
    - [2.3. UDP](#23-udp)
    - [2.4. OAuth](#24-oauth)
    - [2.5. EL](#25-el)
- [3. 底层原理](#3-底层原理)
    - [3.1. 计算机组成](#31-计算机组成)
    - [3.2. 操作系统](#32-操作系统)
    - [3.3. 网络](#33-网络)
    - [3.4. 编译原理](#34-编译原理)
    - [3.5. 数据结构与算法](#35-数据结构与算法)
        - [3.5.1. hash算法](#351-hash算法)
- [4. 通用组件](#4-通用组件)
    - [4.1. 数据库](#41-数据库)
        - [4.1.1. Mysql](#411-mysql)
        - [4.1.2. Oracle](#412-oracle)
    - [4.2. 中间件](#42-中间件)
        - [4.2.1. redis](#421-redis)
            - [4.2.1.1. 数据结构](#4211-数据结构)
        - [4.2.2. mongo](#422-mongo)
        - [4.2.3. Nginx](#423-nginx)
        - [4.2.4. Tomcat](#424-tomcat)
        - [4.2.5. RabbitMq](#425-rabbitmq)
        - [4.2.6. ZooKeeper](#426-zookeeper)
- [5. 技术方案](#5-技术方案)
    - [5.1. SSO](#51-sso)
    - [5.2. 事件总线](#52-事件总线)
    - [5.3. API-Gateway](#53-api-gateway)
    - [5.4. Redis秒杀实现](#54-redis秒杀实现)
    - [5.5. 基础框架项目](#55-基础框架项目)
        - [5.5.1. 调用链](#551-调用链)
        - [5.5.2. 国际化](#552-国际化)
        - [5.5.3. JSON序列化List问题优化](#553-json序列化list问题优化)
        - [5.5.4. AOP事件探针方案](#554-aop事件探针方案)
        - [5.5.5. maven archetype](#555-maven-archetype)
    - [5.6. 服务化改造类项目经验总结](#56-服务化改造类项目经验总结)
    - [5.7. 项目过程相关技能](#57-项目过程相关技能)
        - [5.7.1. git](#571-git)
            - [5.7.1.1. 原理](#5711-原理)
            - [5.7.1.2. 常用命令](#5712-常用命令)
            - [5.7.1.3. 分支模型](#5713-分支模型)
        - [5.7.2. maven](#572-maven)
            - [5.7.2.1. 原理](#5721-原理)
            - [5.7.2.2. 生命周期](#5722-生命周期)
            - [5.7.2.3. 常用插件](#5723-常用插件)
        - [5.7.3. 单元测试](#573-单元测试)
            - [5.7.3.1. junit](#5731-junit)
            - [5.7.3.2. 嵌入式数据库](#5732-嵌入式数据库)
            - [5.7.3.3. mockito](#5733-mockito)
- [6. 开发环境](#6-开发环境)
    - [6.1. 环境搭建](#61-环境搭建)
    - [6.2. IDE配置](#62-ide配置)
    - [6.3. 效率工具](#63-效率工具)
- [7. 语言](#7-语言)
    - [7.1. JAVA](#71-java)
        - [7.1.1. JAVA Language Specification](#711-java-language-specification)
        - [7.1.2. JVM](#712-jvm)
        - [7.1.3. 常用框架](#713-常用框架)
            - [7.1.3.1. Servlet](#7131-servlet)
            - [7.1.3.2. spring mvc](#7132-spring-mvc)
            - [7.1.3.3. SpringFramework](#7133-springframework)
            - [7.1.3.4. mybatis](#7134-mybatis)
            - [7.1.3.5. shiro](#7135-shiro)
            - [7.1.3.6. quartz](#7136-quartz)
            - [7.1.3.7. spring boot](#7137-spring-boot)
        - [7.1.4. 服务化技术栈](#714-服务化技术栈)
            - [7.1.4.1. Dubbo](#7141-dubbo)
            - [7.1.4.2. spring cloud](#7142-spring-cloud)
            - [7.1.4.3. Service Mesh](#7143-service-mesh)
- [8. 质量管理](#8-质量管理)
- [9. 安全](#9-安全)
- [10. PPT与颈椎病康复指南](#10-ppt与颈椎病康复指南)

<!-- /TOC -->

# 1. 理论基础
- [README](01-理论基础/README.md)

# 2. 协议规范
## 2.1. http协议
### 2.1.1. cookie
### 2.1.2. session
### 2.1.3. status code
## 2.2. TCP
- 握手机制建立和关闭会话
- 帧头增加序号
- 重试机制
## 2.3. UDP
## 2.4. OAuth
## 2.5. EL
- http://jinnianshilongnian.iteye.com/blog/2024724
- https://jcp.org/en/jsr/detail?id=341
- gateway解析案例

# 3. 底层原理
## 3.1. 计算机组成


## 3.2. 操作系统

## 3.3. 网络

## 3.4. 编译原理

## 3.5. 数据结构与算法
### 3.5.1. hash算法
-  直接寻址法
```
取关键字或关键字的某个线性函数值为散列地址。即H(key)=key或H(key) = a?key + b，其中a和b为常数（这种散列函数叫做自身函数）
```
-  数字分析法
```
分析一组数据，比如一组员工的出生年月日，这时我们发现出生年月日的前几位数字大体相 同，这样的话，出现冲突的几率就会很大，但是我们发现年月日的后几位表示月份和具体日期的数字差别很大，如果用后面的数字来构成散列地址，则冲突的几率会 明显降低。因此数字分析法就是找出数字的规律，尽可能利用这些数据来构造冲突几率较低的散列地址。
```
- 平方取中法
```
取关键字平方后的中间几位作为散列地址。
```
-  折叠法
```
将关键字分割成位数相同的几部分，最后一部分位数可以不同，然后取这几部分的叠加和（去除进位）作为散列地址。
```
- 随机数法
```
选择一随机函数，取关键字的随机值作为散列地址，通常用于关键字长度不同的场合。
```
- 除留余数法
```
取关键字被某个不大于散列表表长m的数p除后所得的余数为散列地址。即 H(key) = key MOD p, p<=m。不仅可以对关键字直接取模，也可在折叠、平方取中等运算之后取模。对p的选择很重要，一般取素数或m，若p选的不好，容易产生同义词。
```

# 4. 通用组件

## 4.1. 数据库
### 4.1.1. Mysql

### 4.1.2. Oracle

## 4.2. 中间件

### 4.2.1. redis
#### 4.2.1.1. 数据结构
- sds
    ```sds对char进行了包装，使长度计算操作的时间复杂度从O(n)降到O(1),同时减少了字符串追加造成的内存重新分配次数。```
    ```
    typedef char *sds;
    struct sdshdr {
    // buf 已占用长度 
    int len;
    // buf 剩余可用长度 
    int free;
    // 实际保存字符串数据的地方
    char buf[]; };
    ```
- 双端链表
    ```相比于单链表，双端链表多了对尾部的引用```
    - redis实现中，会尽可能使用压缩列表（3.2之后是quicklist）替代双端链表，并在必要的时候转换为双端链表

- 字典
    - key-value结构，redis使用哈希表实现，逻辑类似Java的hashmap
    
- 跳跃表
    - 在链表基础上增加索引层，可以理解为基于二分查找思想，空间换时间的一种思路，索引节点通过随机方式产生，相比于平衡二叉树，少了Rebalance过程。
    - 参考：http://blog.jobbole.com/111731/
    
### 4.2.2. mongo


### 4.2.3. Nginx
### 4.2.4. Tomcat
### 4.2.5. RabbitMq
### 4.2.6. ZooKeeper
- [ZooKeeper原理](04-通用组件/中间件/ZooKeeper/README.md)


# 5. 技术方案
## 5.1. SSO
- 利用cookie解决跨域身份识别
- sdk封装思路
- 安全风险分析

## 5.2. 事件总线
- redis pub-sub 实现消息订阅
- redis list 做队列，实现事件日志采集

## 5.3. API-Gateway
- dsl
- js
- el解析规范
- vfs

## 5.4. Redis秒杀实现
- 整体思路
```
    Redis Watch 命令用于监视一个(或多个) key ，如果在事务执行之前这个(或这些) key 被其他命令所改动，那么事务将被打断
```
- watch流程
- watch实现原理

## 5.5. 基础框架项目
### 5.5.1. 调用链
- Threadlocal
- RPC隐式传参
- logback扩展
### 5.5.2. 国际化
### 5.5.3. JSON序列化List问题优化
- TypeReferenceWrapper解决泛型的类型擦除问题，对序列化框架做一层浅封装
- code template
- 泛型擦除问题分析
### 5.5.4. AOP事件探针方案
- annotation
- aop
- threadlocalTraceId
### 5.5.5. maven archetype
- 生成骨架
- 生成demo模块
## 5.6. 服务化改造类项目经验总结
- 服务分层
- 服务粒度控制
- 分布式事务
    - GTS do&&undo
- 多特性并行开发

## 5.7. 项目过程相关技能
### 5.7.1. git
#### 5.7.1.1. 原理
#### 5.7.1.2. 常用命令
#### 5.7.1.3. 分支模型
### 5.7.2. maven
#### 5.7.2.1. 原理
#### 5.7.2.2. 生命周期
#### 5.7.2.3. 常用插件
### 5.7.3. 单元测试
#### 5.7.3.1. junit
- 基础配置
    - 引入 spring-boot-test-starter，scope=test
    - @RunWith(SpringJUnit4ClassRunner.class)
    - @SpringApplicationConfiguration(classes = {ApplicationTest.class})
    - @Configuration
- 测试代码
    ```java
    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "classpath:h2/clean.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "classpath:h2/init-data.sql")
    })
    ...
    ```
#### 5.7.3.2. 嵌入式数据库
- 配置 test/resources/application.properties
    ```
    spring.datasource.driver-class-name=org.h2.Driver
    spring.datasource.url=jdbc:h2:mem:test;MODE=MYSQL;
    spring.datasource.schema=classpath:h2/init-table.sql
    mybatis.mapper-locations=classpath:sql-mappers/**/*.xml
    ```
#### 5.7.3.3. mockito
- 生成mock bean,可以直接mock接口，不需要有实现类
    ```java
    @Bean
    protected AppInfoCloudService appInfoCloudService() {
        return mock(AppInfoCloudService.class);
    }
    ```
- mock bean调用示例
    ```java
    //mock服务调用行为
    when(appInfoCloudService.getAppInfo(any(Integer.class), any(Integer.class))).thenReturn(APIResponse.success());
    ```
    

# 6. 开发环境
## 6.1. 环境搭建
- [CentOS7搭建开发环境](06-开发环境/01-环境搭建/CentOS7搭建开发环境.md)

## 6.2. IDE配置

## 6.3. 效率工具
- [使用GraphEasy绘制ASCII图]("10-开发环境/使用GraphEasy绘制ASCII图.md")

# 7. 语言
## 7.1. JAVA
### 7.1.1. JAVA Language Specification
- [概述](07-语言/JAVA/JAVA语言规范/README.md)
- 内存模型思路分析
- 各版本语言特性

### 7.1.2. JVM
- [概述](07-语言/JAVA/JVM/README.md)
- 踩坑记录

### 7.1.3. 常用框架
#### 7.1.3.1. Servlet
- 核心组件
    - servlet
    - listener
    - filter
- 内置对象
- 与2.x区别

#### 7.1.3.2. spring mvc
- 运行原理
    - 用户发送请求至前端控制器DispatcherServlet
    - 根据Request信息，匹配handler
    - HandlerAdapter#handle 匹配session 入参 等
    - invokeHandlerMethod
    - Controller执行完成返回ModelAndView。
    - ViewReslover解析后返回具体View。
    - 渲染数据，返回响应

#### 7.1.3.3. SpringFramework
- [基本原理](07-语言/JAVA/SpringFramework/README.md)
- SpringFramework设计模式应用分析

#### 7.1.3.4. mybatis
#### 7.1.3.5. shiro
#### 7.1.3.6. quartz
- 基本配置
- 管理接口实现案例
#### 7.1.3.7. spring boot


### 7.1.4. 服务化技术栈
#### 7.1.4.1. Dubbo
- 内部结构
![](_paste_img/README/2018-03-10-08-51-18.png)
```
dubbo整个框架共分10个层
第三层 proxy、第六层Monitor是两个分界线
三层之前，1、2层的Service和Config是直接面向用户的，用户通过注解、XML等配置服务、服务依赖、配置信息等
三层Proxy对用户的配置进行必要的包装
三层到六层之间，4、5层是服务化的总控，通过4层注册中心、实现整个服务化系统的互通，5层Cluster的特点是以消费端为主
六层Monitor主要做一些信息的收集工作
六层以下是服务调用的落地部分，包括协议、信息交换、数据传输、序列化等
```

#### 7.1.4.2. spring cloud
#### 7.1.4.3. Service Mesh

# 8. 质量管理

# 9. 安全

# 10. PPT与颈椎病康复指南

