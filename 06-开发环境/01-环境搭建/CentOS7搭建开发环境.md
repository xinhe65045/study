## 安装后的配置
### 配置网络
- 打开网络设置
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-17-20-38.png)

- 设置开机自动连接
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-17-21-59.png)

- 设置IPV4
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-17-22-48.png)

### 设置主机名
- 操作命令
    - 查看当前hostname
    ```
        hostnamectl
    ```
    - 设置hostname，重启后也会生效
    ```
        hostnamectl set-hostname xxx
    ```
- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-17-52-42.png)


### 关闭SELinux
- 操作命令
    - 查看selinux状态
    ```
        sestatus
    ```
    - 临时关闭
    ```
        setenforce 0
    ```
    - 永久关闭
    ```shell
        [xinhe@cent7-local ~]$ cat /etc/selinux/config   
    
        # This file controls the state of SELinux on the system.  
        # SELINUX= can take one of these three values:  
        #     enforcing - SELinux security policy is enforced.  
        #     permissive - SELinux prints warnings instead of enforcing.  
        #     disabled - No SELinux policy is loaded.  
        #SELINUX=enforcing  
        SELINUX=disabled  
        # SELINUXTYPE= can take one of three two values:  
        #     targeted - Targeted processes are protected,  
        #     minimum - Modification of targeted policy. Only selected processes are protected.   
        #     mls - Multi Level Security protection.  
        SELINUXTYPE=targeted
        
        [xinhe@cent7-local ~]$ sestatus  
        SELinux status:                 disabled
    ```
- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-17-55-58.png)
- 重启后效果
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-17-58-44.png)

### 关闭防火墙
- 操作命令
    - 查看当前防火墙状态
    ```
        systemctl status firewalld.service
    ```
    - 启用|禁用防火墙
    ```
        sudo systemctl enable firewalld.service 
        sudo systemctl disable firewalld.service 
    ```
    - 启动|停止防火墙
    ``` 
        sudo systemctl start firewalld.service
        sudo systemctl stop firewalld.service
    ```
- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-08-18-04-13.png)

### 配置yum镜像
- 操作命令
    - 备份CentOS-Base.repo
    ```
        sudo mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
    ```
    - 添加repo文件
    ```
        sudo wget http://mirrors.163.com/.help/CentOS7-Base-163.repo
    ```
    - 生成缓存
    ```
        yum clean all
        yum makecache
    ```
- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-13-18-00-05.png)

## 基本开发环境
### developMent tools group
- 操作命令
    - 查看可用组
    ```
        yum groups list 
    ```
    - 安装开发工具组
    ```
        sudo yum groups install "开发工具"
    ```

- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-13-18-18-58.png)
> ![](_paste_img/CentOS7搭建开发环境/2019-03-13-18-19-46.png)

### git2u
- 操作命令
```
    sudo yum install -y epel-release
    sudo rpm -ivh https://centos7.iuscommunity.org/ius-release.rpm  
    yum list git2u
    sudo yum remove git.x86_64 (卸载旧版本，不卸载可能产生冲突)
    sudo yum install -y git2u
    git --version 
```

- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-38-50.png)
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-39-09.png)
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-40-19.png)
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-42-24.png)
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-44-01.png)
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-44-35.png)

- 旧版本冲突截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-15-09-46-39.png)


### jdk
- 操作命令
    - 解压jdk
    ```
        tar xzf /data/file/jdk-8u201-linux-x64.tar.gz -C /data/sdk/
    ```
    - 配置profile
    ```
        vim /etc/profile
        在文件最后添加
            export JAVA_HOME=/data/sdk/jdk1.8.0_201
            export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
            export PATH=$JAVA_HOME/bin:$PATH
            
        source /etc/profile
    ```
    - 验证
    ```
        验证java版本和路径
        java -version
        which java
    ```
- 效果截图
> ![](_paste_img/CentOS7搭建开发环境/2019-03-13-18-35-56.png)

## IDE
### NetBeans


## 其他配置
### 允许当前用户执行sudo
