- uptime
    > ```
    >     15:02  up 3 days,  4:56, 5 users, load averages: 2.16 2.08 2.04
    > ```
    - 其中load averages 三个值分别是 1分钟 5分钟 15分钟的平均负载
        - 当CPU完全空闲的时候，平均负载为0；当CPU工作量饱和的时候，平均负载为1。
        - 每个CPU核心可以处理1个负载
        - 比如8核CPU的系统，负载为8时表示满负载运行，再高就偏高了

- 获取系统已运行时间
cat /proc/uptime| awk -F. '{run_days=$1 / 86400;run_hour=($1 % 86400)/3600;run_minute=($1 % 3600)/60;run_second=$1 % 60;printf("系统已运行：%d天%d时%d分%d秒",run_days,run_hour,run_minute,run_second)}'


查询ip地址与主机名
ifconfig | grep "addr:" | grep -v 127.0.0.1 | awk '{split($0,arr);print arr[2]}'| awk '{split($0,arr1,":");printf arr1[2]" "}' && hostname

查询日志文件中某个字符串出现的次数（其中$1/3表示）
for i in `ls | grep catalina-`
    do
        echo "$i" | awk '{printf substr($0,10,length-13)"访问次数: "}'
        cat "$i" | grep dcs. |wc -l | awk '{print int($1/3)}'
    done


linux下获取占用CPU资源最多的10个进程，可以使用如下命令组合：
ps aux|head -1;ps aux|grep -v PID|sort -rn -k +3|head


linux下获取占用内存资源最多的10个进程，可以使用如下命令组合：
ps aux|head -1;ps aux|grep -v PID|sort -rn -k +4|head

查询当前连接某个端口的进程
netstat -nalp | grep 6667 | awk '{print $7}' | awk '{split($0,a,"/");print a[1]}'  |awk '!a[$1]++{print}' |awk '{print "ps aux | grep "$1 "|grep -v grep"}'




批量全局安装
for i in `ls /app/evun/sweet/sweet-ui/node_modules/`
    do
        if [ -a "/usr/local/lib/node_modules/$i" ]; then 
            echo "$i global installed"
        else
        	echo "$i global installing"
            npm install -g "$i"
        fi
    done

生成mybatis mapper 列表
# 1、cd mybatis 目录
for i in `ls .`
    do
        echo  "<mapper resource=\"META-INF/mybatis/$i\"/>"
    done

批量清理maven
for i in `ls .`
    do
        if [[ -d $i ]]; then
            cd $i
            mvn clean
            cd ..
        fi
    done



