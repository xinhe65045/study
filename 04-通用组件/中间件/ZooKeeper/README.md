
- zookeeper原理
    ```
    ZooKeeper是一种为分布式应用所设计的高可用、高性能且一致的开源协调服务，它提供了一项基本服务：分布式锁服务。由于ZooKeeper的开源特性，后来我们的开发者在分布式锁的基础上，摸索了出了其他的使用方法：配置维护、组服务、分布式消息队列、分布式通知/协调等。
     ZooKeeper所提供的服务主要是通过：数据结构+原语+watcher机制，三个部分来实现的。
    ```

- znode结构
    ```
    ZooKeeper命名空间中的Znode，兼具文件和目录两种特点。既像文件一样维护着数据、元信息、ACL、时间戳等数据结构，又像目录一样可以作为路径标识的一部分。图中的每个节点称为一个Znode。 每个Znode由3部分组成:
    ① stat：此为状态信息, 描述该Znode的版本, 权限等信息
    ② data：与该Znode关联的数据
    ③ children：该Znode下的子节点
    znode创建类型(CreateMode),有以下四种：
        PERSISTENT                持久化节点
        PERSISTENT_SEQUENTIAL     顺序自动编号持久化节点，这种节点会根据当前已存在的节点数自动加 1
        EPHEMERAL                 临时节点， 客户端session超时这类节点就会被自动删除
        EPHEMERAL_SEQUENTIAL      临时自动编号节点
    ```
    
- 分布式锁获取过程
    ![](_paste_img/README/2018-03-10-09-24-48.png)
    ```
    客户端调用create()方法创建名为“locknode/guid-lock-”的节点，需要注意的是，这里节点的创建类型需要设置为EPHEMERAL_SEQUENTIAL。
    客户端调用getChildren(“locknode”)方法来获取所有已经创建的子节点，同时在这个节点上注册上子节点变更通知的Watcher。
    客户端获取到所有子节点path之后，如果发现自己在步骤1中创建的节点是所有节点中序号最小的，那么就认为这个客户端获得了锁。
    如果在步骤3中发现自己并非是所有子节点中最小的，说明自己还没有获取到锁，就开始等待，直到下次子节点变更通知的时候，再进行子节点的获取，判断是否获取锁。
    ```
